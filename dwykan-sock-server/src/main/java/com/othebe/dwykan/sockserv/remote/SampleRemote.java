package com.othebe.dwykan.sockserv.remote;

import com.messages.BaseMessage;
import com.messages.MessageType;
import com.messages.message_types.ClientAddVideoMessage;
import com.messages.message_types.ClientJoinRoomMessage;
import com.messages.message_types.ClientPauseVideoMessage;
import com.messages.message_types.ClientPlayVideoMessage;
import com.messages.message_types.ClientSeekVideoMessage;
import com.messages.message_types.ClientSetHandleMessage;
import com.othebe.dwykan.mina.MessageCodecFactory;
import com.othebe.dwykan.sockserv.common.Constants;

import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.net.InetSocketAddress;
import java.util.Scanner;

public class SampleRemote implements IServerActionHandler {
  public static void main(String[] args) {
    SampleRemote sampleRemote = new SampleRemote(Constants.PORT);
    sampleRemote.connect();
  }

  /***************
   * BEGIN.
   */
  private final Repl repl;
  private final int port;

  private IoSession ioSession;

  public SampleRemote(int port) {
    this.port = port;
    this.repl = new Repl(this);
  }

  public void connect() {
    IoConnector ioConnector = new NioSocketConnector();
    ioConnector.getFilterChain().addFirst("protocol", new ProtocolCodecFilter(new MessageCodecFactory()));
    ioConnector.setHandler(new IoHandlerAdapter() {
      @Override
      public void sessionOpened(IoSession session) throws Exception {
        ioSession = session;
      }

      @Override
      public void messageSent(IoSession session, Object message) throws Exception {
        super.messageSent(session, message);
        System.out.println("Message sent " + message.toString());
      }

      @Override
      public void messageReceived(IoSession session, Object message) throws Exception {
        System.out.println("Message recvd " + message.toString());
        BaseMessage baseMessage = (BaseMessage) message;

        if (baseMessage.getMessageType() == MessageType.ACK_CLIENT) {
          Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
              repl.start();
            }
          });
          thread.start();
        }
      }

      @Override
      public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        System.out.println(cause.getMessage());
      }
    });
    ioConnector.connect(new InetSocketAddress(port));
  }

  @Override
  public void setHandle(String handle) {
    try {
      ioSession.write(new ClientSetHandleMessage(handle));
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  @Override
  public void joinRoom(long roomId) {
    ioSession.write(new ClientJoinRoomMessage(roomId));
  }

  @Override
  public void addVideoId(String videoId) {
    ioSession.write(new ClientAddVideoMessage(videoId, 1 /** Youtube */));
  }

  @Override
  public void playVideoId(String videoId) {
    ioSession.write(new ClientPlayVideoMessage(videoId));
  }

  @Override
  public void pauseVideo() {
    ioSession.write(new ClientPauseVideoMessage());
  }

  @Override
  public void seekVideo(int seekTo) {
    ioSession.write(new ClientSeekVideoMessage(seekTo));
  }
}

