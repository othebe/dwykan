package com.othebe.dwykan.sockserv.managers.room;

import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan.sockserv.managers.client.ClientManager;
import java.util.concurrent.ConcurrentHashMap;

public class RoomManager {
  private ConcurrentHashMap<Long, Room> rooms;
  private ClientManager clientManager;

  public RoomManager(ClientManager clientManager) {
    this.rooms = new ConcurrentHashMap<>();
    this.clientManager = clientManager;
  }

  /**
   * Determines if a room exists.
   * @param id
   * @return
   */
  public boolean hasExistingRoom(long id) {
    return rooms.containsKey(id);
  }

  /**
   * Gets a room.
   * @param id
   * @return
   */
  public Room getRoom(long id) {
    return rooms.get(id);
  }

  /**
   * Creates a room from ID. Returns room if already exists.
   * @param id
   * @return
   */
  public Room createRoom(long id) {
    if (rooms.containsKey(id)) {
      return rooms.get(id);
    } else {
      Room room = new Room(id);
      room.setRoomUpdateHandler(new RoomUpdateHandler(room, clientManager));
      rooms.put(id, room);
      return room;
    }
  }

  public void removeRoom(long id) {
    Room room = getRoom(id);
    if (room != null && room.getConnectedClients().size() == 0) {
      rooms.remove(id);
    }
  }
}
