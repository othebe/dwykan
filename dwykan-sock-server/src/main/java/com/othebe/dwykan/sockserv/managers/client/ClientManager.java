package com.othebe.dwykan.sockserv.managers.client;

import com.othebe.dwykan.models.client.Client;

import org.apache.mina.core.session.IoSession;

import java.util.concurrent.ConcurrentHashMap;

public class ClientManager {
  private ConcurrentHashMap<Client, IoSession> clientSessionMap;

  public ClientManager() {
    this.clientSessionMap = new ConcurrentHashMap<>();
  }

  public void addClientSession(Client client, IoSession session) {
    clientSessionMap.put(client, session);
  }

  public void removeClientSession(Client client) {
    clientSessionMap.remove(client);
  }

  public IoSession getSession(Client client) {
    return clientSessionMap.get(client);
  }
}
