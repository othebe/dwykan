package com.othebe.dwykan.sockserv.remote;

public interface IServerActionHandler {
  public void connect();
  public void setHandle(String handle);
  public void joinRoom(long roomId);
  public void addVideoId(String videoId);
  public void playVideoId(String videoId);
  public void pauseVideo();
  public void seekVideo(int seekTo);
}
