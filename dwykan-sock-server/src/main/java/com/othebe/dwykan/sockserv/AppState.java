package com.othebe.dwykan.sockserv;

import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan.sockserv.managers.client.ClientManager;
import com.othebe.dwykan.sockserv.managers.room.RoomManager;

import java.util.concurrent.ConcurrentHashMap;

public class AppState {
  private static final AppState instance = new AppState();

  private ClientManager clientManager;
  private RoomManager roomManager;

  public static AppState getInstance() {
    return instance;
  }

  public ClientManager getClientManager() {
    if (clientManager == null) {
      clientManager = new ClientManager();
    }
    return clientManager;
  }

  public RoomManager getRoomManager() {
    if (roomManager == null) {
      roomManager = new RoomManager(getClientManager());
    }
    return roomManager;
  }
}
