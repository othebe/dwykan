package com.othebe.dwykan.sockserv.managers.room;

import com.messages.message_types.ServerAddVideoMessage;
import com.messages.message_types.ServerBufferingVideoMessage;
import com.messages.message_types.ServerJoinRoomMessage;
import com.messages.message_types.ServerLeaveRoomMessage;
import com.messages.message_types.ServerPauseVideoMessage;
import com.messages.message_types.ServerPlayVideoMessage;
import com.messages.message_types.ServerSeekVideoMessage;
import com.messages.message_types.ServerStopVideoMessage;
import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.IRoomUpdateHandler;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan.sockserv.managers.client.ClientManager;

import org.apache.mina.core.session.IoSession;

public class RoomUpdateHandler implements IRoomUpdateHandler {
  private final Room room;
  private final ClientManager clientManager;

  // TODO (othebe): Move this to RoomUpdateNotifier to perform throttling.
  public RoomUpdateHandler(Room room, ClientManager clientManager) {
    this.room = room;
    this.clientManager = clientManager;
  }

  @Override
  public void onAddClient(Client client) {
    ServerJoinRoomMessage message = new ServerJoinRoomMessage(client);
    for (Client c : room.getConnectedClients()) {
      if (client != c) {
        IoSession ioSession = clientManager.getSession(c);
        ioSession.write(message);
      }
    }
  }

  @Override
  public void onRemoveClient(Client client) {
    ServerLeaveRoomMessage message = new ServerLeaveRoomMessage(client);
    for (Client c : room.getConnectedClients()) {
      if (client != c) {
        IoSession ioSession = clientManager.getSession(c);
        ioSession.write(message);
      }
    }
  }

  @Override
  public void onAddVideo(String url) {
    ServerAddVideoMessage message = new ServerAddVideoMessage(url);
    for (Client c : room.getConnectedClients()) {
      clientManager.getSession(c).write(message);
    }
  }

  @Override
  public void onBuffering() {
    ServerBufferingVideoMessage message = new ServerBufferingVideoMessage();
    for (Client c : room.getConnectedClients()) {
      clientManager.getSession(c).write(message);
    }
  }

  @Override
  public void onPauseVideo() {
    ServerPauseVideoMessage message = new ServerPauseVideoMessage();
    for (Client c : room.getConnectedClients()) {
      clientManager.getSession(c).write(message);
    }
  }

  @Override
  public void onPlayVideo(String url) {
    ServerPlayVideoMessage message = new ServerPlayVideoMessage(url);
    for (Client c : room.getConnectedClients()) {
      clientManager.getSession(c).write(message);
    }
  }

  @Override
  public void onSeekVideo(int seekTo) {
    ServerSeekVideoMessage message = new ServerSeekVideoMessage(seekTo);
    for (Client c : room.getConnectedClients()) {
      clientManager.getSession(c).write(message);
    }
  }

  @Override
  public void onStopVideo() {
    ServerStopVideoMessage message = new ServerStopVideoMessage();
    for (Client c : room.getConnectedClients()) {
      clientManager.getSession(c).write(message);
    }
  }
}
