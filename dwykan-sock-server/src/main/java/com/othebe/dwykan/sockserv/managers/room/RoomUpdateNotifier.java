package com.othebe.dwykan.sockserv.managers.room;

import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.Room;

import org.apache.mina.core.session.IoSession;

import java.util.concurrent.ConcurrentHashMap;

public class RoomUpdateNotifier {
  private ConcurrentHashMap<Client, IoSession> clientSessionMap;

  public RoomUpdateNotifier(ConcurrentHashMap<Client, IoSession> clientSessionMap) {
    this.clientSessionMap = clientSessionMap;
  }

  public void onAddClient(Room room, Client client) {

  }
}
