package com.othebe.dwykan.sockserv;

import com.messages.message_types.AckClientMessage;
import com.messages.IMessage;
import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan.sockserv.common.Constants;
import com.othebe.dwykan.sockserv.common.SafeExceptionHandler;
import com.othebe.dwykan.sockserv.managers.client.ClientManager;
import com.othebe.dwykan.sockserv.managers.room.RoomManager;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by otheb on 4/28/2016.
 */
public class Handler extends IoHandlerAdapter {
  private RoomManager roomManager;
  private ClientManager clientManager;

  public Handler() {
    this.roomManager = AppState.getInstance().getRoomManager();
    this.clientManager = AppState.getInstance().getClientManager();
  }

  @Override
  public void sessionOpened(IoSession session) throws Exception {
    try {
      // Create new Client.
      Client client = new Client();
      clientManager.addClientSession(client, session);
      session.setAttribute(Constants.SESSION_CLIENT_KEY, client);

      // Respond.
      session.write(new AckClientMessage());
      System.out.println("Session opened");
    } catch (Exception e) {
      SafeExceptionHandler.handle(e);
    }
  }

  @Override
  public void sessionClosed(IoSession session) throws Exception {
    Client client = (Client) session.getAttribute(Constants.SESSION_CLIENT_KEY);

    //Clean room if no more clients.
    long roomId = client.getRoomId();
    RoomManager roomManager = AppState.getInstance().getRoomManager();
    Room room = roomManager.getRoom(roomId);
    if (room != null) {
      room.removeClient(client);
      if (room.isRoomEmpty()) {
        roomManager.removeRoom(roomId);
      }
    }

    clientManager.removeClientSession(client);
  }

  @Override
  public void messageReceived(IoSession session, Object message) {
    try {
      System.out.println("Message recvd - " + message.toString());
      MessageReceivedDelegate.handle(session, (IMessage) message);
    } catch (Exception e) {
      SafeExceptionHandler.handle(e);
    }
  }

  @Override
  public void messageSent(IoSession session, Object message) throws Exception {
    super.messageSent(session, message);
    System.out.println("Message sent - " + message.toString());
  }

  @Override
  public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
    System.out.println(cause.getMessage());
  }
}
