package com.othebe.dwykan.sockserv.remote;

import java.util.Scanner;

public class Repl {
  private enum ReplState {
    MAIN_MENU,
    SET_HANDLE,
    JOIN_ROOM,
    ADD_VIDEO,
    PLAY_VIDEO,
    PAUSE_VIDEO,
    SEEK_VIDEO
  }

  private IServerActionHandler serverActionHandler;
  private ReplState replState;

  public Repl(IServerActionHandler serverActionHandler) {
    this.serverActionHandler = serverActionHandler;
    this.replState = ReplState.MAIN_MENU;
  }

  public void start() {
    show();
  }

  private void show() {
    System.out.println();
    switch(replState) {
      case MAIN_MENU:
        showMainMenu();
        break;
      case SET_HANDLE:
        showSetHandle();
        break;
      case JOIN_ROOM:
        showJoinRoom();
        break;
      case ADD_VIDEO:
        showAddVideo();
        break;
      case PLAY_VIDEO:
        showPlayVideo();
        break;
      case PAUSE_VIDEO:
        showPauseVideo();
        break;
      case SEEK_VIDEO:
        showSeekVideo();
        break;
    }
    show();
  }

  private void showMainMenu() {
    System.out.println("1. Set handle.");
    System.out.println("2. Join room.");
    System.out.println("3. Add video.");
    System.out.println("4. Play video.");
    System.out.println("5. Pause video.");
    System.out.println("6. Seek video.");

    int choice = new Scanner(System.in).nextInt();
    switch(choice) {
      case 1:
        replState = ReplState.SET_HANDLE;
        break;
      case 2:
        replState = ReplState.JOIN_ROOM;
        break;
      case 3:
        replState = ReplState.ADD_VIDEO;
        break;
      case 4:
        replState = ReplState.PLAY_VIDEO;
        break;
      case 5:
        replState = ReplState.PAUSE_VIDEO;
        break;
      case 6:
        replState = ReplState.SEEK_VIDEO;
        break;
    }
  }

  private void showSetHandle() {
    System.out.println("Handle? ");
    serverActionHandler.setHandle(new Scanner(System.in).nextLine());
    replState = ReplState.MAIN_MENU;
  }

  private void showJoinRoom() {
    System.out.println("Room? ");
    serverActionHandler.joinRoom(new Scanner(System.in).nextLong());
    replState = ReplState.MAIN_MENU;
  }

  private void showAddVideo() {
    System.out.println("Video ID? ");
    serverActionHandler.addVideoId(new Scanner(System.in).nextLine());
    replState = ReplState.MAIN_MENU;
  }

  private void showPlayVideo() {
    System.out.println("Video ID? ");
    serverActionHandler.playVideoId(new Scanner(System.in).nextLine());
    replState = ReplState.MAIN_MENU;
  }

  private void showPauseVideo() {
    serverActionHandler.pauseVideo();
    replState = ReplState.MAIN_MENU;
  }

  private void showSeekVideo() {
    System.out.println("Seek to? ");
    serverActionHandler.seekVideo(new Scanner(System.in).nextInt());
    replState = ReplState.MAIN_MENU;
  }
}
