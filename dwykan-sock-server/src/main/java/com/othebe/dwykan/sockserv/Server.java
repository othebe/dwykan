package com.othebe.dwykan.sockserv;

import com.othebe.dwykan.mina.MessageCodecFactory;
import com.othebe.dwykan.sockserv.common.Constants;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Server {
  public static void main(String[] args) throws IOException {
    IoAcceptor ioAcceptor = new NioSocketAcceptor();
    ioAcceptor.getFilterChain().addFirst("protocol", new ProtocolCodecFilter(new MessageCodecFactory()));
    ioAcceptor.setHandler(new Handler());

    ioAcceptor.bind(new InetSocketAddress(Constants.PORT));
  }
}
