package com.othebe.dwykan.sockserv;

import com.messages.message_types.AckAddVideoMessage;
import com.messages.message_types.AckJoinRoomMessage;
import com.messages.message_types.AckSetHandleMessage;
import com.messages.message_types.ClientAddVideoMessage;
import com.messages.message_types.ClientJoinRoomMessage;
import com.messages.message_types.ClientPauseVideoMessage;
import com.messages.message_types.ClientPlayVideoMessage;
import com.messages.message_types.ClientSeekVideoMessage;
import com.messages.message_types.ClientSetHandleMessage;
import com.messages.IMessage;
import com.messages.MessageType;
import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan.sockserv.AppState;
import com.othebe.dwykan.sockserv.common.Constants;
import com.othebe.dwykan.sockserv.managers.room.RoomManager;

import org.apache.mina.core.session.IoSession;

/**
 * Created by otheb on 4/28/2016.
 */
public class MessageReceivedDelegate {
  public static final AppState appState = AppState.getInstance();

  public static void handle(IoSession ioSession, IMessage message) {
    // Client set handle.
    if (message.getMessageType() == MessageType.CLIENT_SET_HANDLE) {
      handle(ioSession, (ClientSetHandleMessage) message);
    }
    // Client join room.
    else if (message.getMessageType() == MessageType.CLIENT_JOIN_ROOM) {
      handle(ioSession, (ClientJoinRoomMessage) message);
    }
    // Add video.
    else if (message.getMessageType() == MessageType.CLIENT_ADD_VIDEO) {
      handle(ioSession, (ClientAddVideoMessage) message);
    }
    // Play video.
    else if (message.getMessageType() == MessageType.CLIENT_PLAY_VIDEO) {
      handle(ioSession, (ClientPlayVideoMessage) message);
    }
    // Pause video.
    else if (message.getMessageType() == MessageType.CLIENT_PAUSE_VIDEO) {
      handle(ioSession, (ClientPauseVideoMessage) message);
    }
    // Seek video.
    else if (message.getMessageType() == MessageType.CLIENT_SEEK_VIDEO) {
      handle(ioSession, (ClientSeekVideoMessage) message);
    }
  }

  /**
   * Handle client setting handle.
   * @param session
   * @param message
   */
  private static void handle(IoSession session, ClientSetHandleMessage message) {
    Client client = (Client) session.getAttribute(Constants.SESSION_CLIENT_KEY);
    client.setHandle(message.getHandle());
    session.setAttribute(Constants.SESSION_CLIENT_KEY, client);
    session.write(new AckSetHandleMessage());
  }

  /**
   * Handle client joining a room.
   * @param session
   * @param message
   */
  private static void handle(IoSession session, ClientJoinRoomMessage message) {
    // Find or create a room.
    long roomId = message.getRoomId();
    Room room = appState.getRoomManager().createRoom(roomId);

    Client client = (Client) session.getAttribute(Constants.SESSION_CLIENT_KEY);
    client.setRoomId(roomId);
    room.addClient(client);
    session.setAttribute(Constants.SESSION_CLIENT_KEY, client);

    session.write(new AckJoinRoomMessage(room));
  }

  /**
   * Handle client adding a video.
   * @param session
   * @param message
   */
  private static void handle(IoSession session, ClientAddVideoMessage message) {
    Client client = (Client) session.getAttribute(Constants.SESSION_CLIENT_KEY);
    Room room = appState.getRoomManager().getRoom(client.getRoomId());
    if (room == null) {
      return;
    }

    room.getRoomVideos().addVideo(message.getVideoUrl());

    session.write(new AckAddVideoMessage(room));
  }

  private static void handle(IoSession session, ClientPlayVideoMessage message) {
    Client client = (Client) session.getAttribute(Constants.SESSION_CLIENT_KEY);
    Room room = appState.getRoomManager().getRoom(client.getRoomId());
    if (room == null) {
      return;
    }

    room.getRoomVideos().playVideo(message.getVideoUrl());
  }

  private static void handle(IoSession session, ClientPauseVideoMessage message) {
    Client client = (Client) session.getAttribute(Constants.SESSION_CLIENT_KEY);
    Room room = appState.getRoomManager().getRoom(client.getRoomId());
    if (room == null) {
      return;
    }

    room.getRoomVideos().pauseVideo();
  }

  private static void handle(IoSession session, ClientSeekVideoMessage message) {
    Client client = (Client) session.getAttribute(Constants.SESSION_CLIENT_KEY);
    Room room = appState.getRoomManager().getRoom(client.getRoomId());
    if (room == null) {
      return;
    }

    room.getRoomVideos().seekVideoTo(message.getSeek());
  }
}
