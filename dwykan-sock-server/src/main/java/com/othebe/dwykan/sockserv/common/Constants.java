package com.othebe.dwykan.sockserv.common;

public class Constants {
  public static final int PORT = 8085;

  public static final String SESSION_CLIENT_KEY = "session-client-key";
}
