package com.othebe.dwykan.models.room;

public enum PlayState {
    PLAYING,
    SEEKING,
    PAUSED,
    STOPPED,
    BUFFERING
}
