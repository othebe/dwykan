package com.othebe.dwykan.models.client;

/**
 * Created by otheb on 7/11/2016.
 */
public class Client {
  private String handle;
  private long roomId;

  public Client setHandle(String handle) {
    this.handle = handle;
    return this;
  }

  public Client setRoomId(long roomId) {
    this.roomId = roomId;
    return this;
  }

  public String getHandle() { return handle; }

  public long getRoomId() {
    return roomId;
  }
}
