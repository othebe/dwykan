package com.othebe.dwykan.models.room;

import com.othebe.dwykan.models.client.Client;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by otheb on 7/11/2016.
 */
public class Room {
  private long id;
  private HashMap<Client, Boolean> connectedClients;
  private IRoomUpdateHandler roomUpdateHandler;

  private RoomVideos roomVideos;

  public Room(long id) {
    this.id = id;
    this.connectedClients = new HashMap<>();
    this.roomVideos = new RoomVideos();
  }

  public void setRoomUpdateHandler(IRoomUpdateHandler roomUpdateHandler) {
    this.roomUpdateHandler = roomUpdateHandler;
    this.roomVideos.setRoomUpdateHandler(roomUpdateHandler);
  }

  /**
   * Set room ID.
   * @param id
   */
  public void setId(long id) {
    this.id = id;
  }

  /**
   * Get room ID.
   * @return
   */
  public long getId() {
    return id;
  }

  /**
   * Add a client to the room.
   * @param client
   */
  public void addClient(Client client) {
    connectedClients.put(client, true);

    if (roomUpdateHandler != null) {
      roomUpdateHandler.onAddClient(client);
    }
  }

  /**
   * Remove a client from the room.
   * @param client
   */
  public void removeClient(Client client) {
    connectedClients.remove(client);

    if (roomUpdateHandler != null) {
      roomUpdateHandler.onRemoveClient(client);
    }
  }

  /**
   * Determines if the room is empty.
   * @return
   */
  public boolean isRoomEmpty() { return connectedClients.isEmpty(); }

  /**
   * Get clients that are connected to this room.
   * @return
   */
  public Set<Client> getConnectedClients() {
    return connectedClients.keySet();
  }

  /**
   * Get the room videos object.
   * @return
   */
  public RoomVideos getRoomVideos() {
    return roomVideos;
  }
}
