package com.othebe.dwykan.models.room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class RoomVideos {
  private IRoomUpdateHandler roomUpdateHandler;
  private ConcurrentLinkedQueue<String> videoUrls;

  private PlayState playState;
  private String currentUrl;
  private int currentSeek;

  public RoomVideos() {
    this.videoUrls = new ConcurrentLinkedQueue<>();
    this.playState = PlayState.STOPPED;
  }

  public void setRoomUpdateHandler(IRoomUpdateHandler roomUpdateHandler) {
    this.roomUpdateHandler = roomUpdateHandler;
  }

  public List<String> getVideoUrls() {
    ArrayList<String> urlList = new ArrayList<>();
    urlList.addAll(videoUrls);

    return urlList;
  }

  public void setCurrentUrl(String url) {
    currentUrl = url;
  }

  public String getCurrentUrl() {
    return currentUrl;
  }

  public PlayState getPlayState() {
    return playState;
  }

  /**
   * Add a video.
   * @param url
   */
  public void addVideo(String url) {
    if (!videoUrls.contains(url)) {
      videoUrls.add(url);
    }

    if (roomUpdateHandler != null) {
      roomUpdateHandler.onAddVideo(url);
    }
  }

  /**
   * Play a video.
   * @param url
   */
  public void playVideo(String url) {
    if (videoUrls.contains(url)) {
      currentUrl = url;
      playState = PlayState.PLAYING;
    }

    if (roomUpdateHandler != null) {
      roomUpdateHandler.onPlayVideo(url);
    }
  }

  /**
   * Pause currently playing video.
   */
  public void pauseVideo() {
    if (playState == PlayState.PLAYING) {
      playState = PlayState.PAUSED;
    }
    if (roomUpdateHandler != null) {
      roomUpdateHandler.onPauseVideo();
    }
  }

  /**
   * Stop playing video.
   */
  public void stopVideo() {
    currentUrl = "";
    currentSeek = 0;
    playState = PlayState.STOPPED;

    if (roomUpdateHandler != null) {
      roomUpdateHandler.onStopVideo();
    }
  }

  /**
   * Video is buffering.
   */
  public void bufferingVideo() {
    if (roomUpdateHandler != null) {
      roomUpdateHandler.onBuffering();
    }
  }

  /**
   * Seek video to.
   * @param seekTo
   */
  public void seekVideoTo(int seekTo) {
    if (currentUrl == null || currentUrl.length() == 0) {
      return;
    }

    if (playState == PlayState.STOPPED) {
      return;
    }

    currentSeek = seekTo;
    playState = PlayState.SEEKING;

    if (roomUpdateHandler != null) {
      roomUpdateHandler.onSeekVideo(seekTo);
    }
  }
}
