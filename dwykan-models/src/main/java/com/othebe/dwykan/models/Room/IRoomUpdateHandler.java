package com.othebe.dwykan.models.room;

import com.othebe.dwykan.models.client.Client;

public interface IRoomUpdateHandler {
  // Client stuff.
  void onAddClient(Client client);
  void onRemoveClient(Client client);

  // Video stuff.
  void onAddVideo(String url);
  void onBuffering();
  void onPauseVideo();
  void onPlayVideo(String url);
  void onSeekVideo(int seekTo);
  void onStopVideo();
}
