package com.othebe.dwykan.mina;

import com.google.gson.Gson;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.filter.codec.textline.TextLineEncoder;

/**
 * Created by otheb on 4/27/2016.
 */
public class MessageEncoder extends TextLineEncoder {
  @Override
  public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
    Gson gson = new Gson();

    super.encode(session, gson.toJson(message), out);
  }
}
