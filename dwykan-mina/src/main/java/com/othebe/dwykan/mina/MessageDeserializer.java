package com.othebe.dwykan.mina;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.messages.IMessage;
import com.messages.MessageType;
import com.messages.message_types.AckAddVideoMessage;
import com.messages.message_types.AckBufferingVideoMessage;
import com.messages.message_types.AckClientMessage;
import com.messages.message_types.AckJoinRoomMessage;
import com.messages.message_types.AckPauseVideoMessage;
import com.messages.message_types.AckPlayVideoMessage;
import com.messages.message_types.AckSeekVideoMessage;
import com.messages.message_types.AckSetHandleMessage;
import com.messages.message_types.AckStopVideoMessage;
import com.messages.message_types.ClientAddVideoMessage;
import com.messages.message_types.ClientBufferingVideoMessage;
import com.messages.message_types.ClientJoinRoomMessage;
import com.messages.message_types.ClientPauseVideoMessage;
import com.messages.message_types.ClientPlayVideoMessage;
import com.messages.message_types.ClientSeekVideoMessage;
import com.messages.message_types.ClientSetHandleMessage;
import com.messages.message_types.ClientStopVideoMessage;
import com.messages.message_types.ServerAddVideoMessage;
import com.messages.message_types.ServerBufferingVideoMessage;
import com.messages.message_types.ServerJoinRoomMessage;
import com.messages.message_types.ServerLeaveRoomMessage;
import com.messages.message_types.ServerPauseVideoMessage;
import com.messages.message_types.ServerPlayVideoMessage;
import com.messages.message_types.ServerSeekVideoMessage;
import com.messages.message_types.ServerStopVideoMessage;

import java.lang.reflect.Type;

/**
 * Created by otheb on 4/28/2016.
 */
public class MessageDeserializer implements JsonDeserializer<IMessage> {
  @Override
  public IMessage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
    Gson gson = new Gson();
    JsonObject jsonObject = json.getAsJsonObject();
    JsonElement messageTypeElt = jsonObject.get("messageType");

    switch (MessageType.valueOf(messageTypeElt.getAsString())) {
      case ACK_CLIENT:
        return gson.fromJson(json, AckClientMessage.class);
      case CLIENT_SET_HANDLE:
        return gson.fromJson(json, ClientSetHandleMessage.class);
      case ACK_SET_HANDLE:
        return gson.fromJson(json, AckSetHandleMessage.class);
      case CLIENT_JOIN_ROOM:
        return gson.fromJson(json, ClientJoinRoomMessage.class);
      case ACK_JOIN_ROOM:
        return gson.fromJson(json, AckJoinRoomMessage.class);
      case SERVER_JOIN_ROOM:
        return gson.fromJson(json, ServerJoinRoomMessage.class);
      case SERVER_LEAVE_ROOM:
        return gson.fromJson(json, ServerLeaveRoomMessage.class);
      case CLIENT_ADD_VIDEO:
        return gson.fromJson(json, ClientAddVideoMessage.class);
      case ACK_ADD_VIDEO:
        return gson.fromJson(json, AckAddVideoMessage.class);
      case SERVER_ADD_VIDEO:
        return gson.fromJson(json, ServerAddVideoMessage.class);
      case CLIENT_SEEK_VIDEO:
        return gson.fromJson(json, ClientSeekVideoMessage.class);
      case ACK_SEEK_VIDEO:
        return gson.fromJson(json, AckSeekVideoMessage.class);
      case SERVER_SEEK_VIDEO:
        return gson.fromJson(json, ServerSeekVideoMessage.class);
      case CLIENT_BUFFERING_VIDEO:
        return gson.fromJson(json, ClientBufferingVideoMessage.class);
      case ACK_BUFFERING_VIDEO:
        return gson.fromJson(json, AckBufferingVideoMessage.class);
      case SERVER_BUFFERING_VIDEO:
        return gson.fromJson(json, ServerBufferingVideoMessage.class);
      case CLIENT_PLAY_VIDEO:
        return gson.fromJson(json, ClientPlayVideoMessage.class);
      case ACK_PLAY_VIDEO:
        return gson.fromJson(json, AckPlayVideoMessage.class);
      case SERVER_PLAY_VIDEO:
        return gson.fromJson(json, ServerPlayVideoMessage.class);
      case CLIENT_PAUSE_VIDEO:
        return gson.fromJson(json, ClientPauseVideoMessage.class);
      case ACK_PAUSE_VIDEO:
        return gson.fromJson(json, AckPauseVideoMessage.class);
      case SERVER_PAUSE_VIDEO:
        return gson.fromJson(json, ServerPauseVideoMessage.class);
      case CLIENT_STOP_VIDEO:
        return gson.fromJson(json, ClientStopVideoMessage.class);
      case ACK_STOP_VIDEO:
        return gson.fromJson(json, AckStopVideoMessage.class);
      case SERVER_STOP_VIDEO:
        return gson.fromJson(json, ServerStopVideoMessage.class);
    }

    throw new JsonParseException("Unhandled message type [" + messageTypeElt.getAsString() + "]");
  }
}
