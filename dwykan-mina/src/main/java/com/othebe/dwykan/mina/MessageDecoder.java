package com.othebe.dwykan.mina;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.messages.BaseMessage;
import com.messages.IMessage;
import com.messages.MessageType;
import com.messages.message_types.AckClientMessage;
import com.messages.message_types.ClientSetHandleMessage;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.textline.TextLineDecoder;

import java.lang.reflect.Type;

/**
 * Created by otheb on 4/27/2016.
 */
public class MessageDecoder extends TextLineDecoder {
  @Override
  protected void writeText(IoSession session, String text, ProtocolDecoderOutput out) {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeAdapter(IMessage.class, new MessageDeserializer());
    Gson gson = gsonBuilder.create();

    IMessage message = gson.fromJson(text, IMessage.class);
    if (message != null) {
      out.write(message);
    }
  }
}
