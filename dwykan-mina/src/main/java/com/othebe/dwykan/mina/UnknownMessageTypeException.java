package com.othebe.dwykan.mina;

/**
 * Created by otheb on 4/30/2016.
 */
public class UnknownMessageTypeException extends Exception {
  public UnknownMessageTypeException(String message) {
    super(message);
  }
}
