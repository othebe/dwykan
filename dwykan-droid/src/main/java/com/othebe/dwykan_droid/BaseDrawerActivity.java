package com.othebe.dwykan_droid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.othebe.dwykan_droid.ui.search.SearchFragment;

public abstract class BaseDrawerActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
  private SearchFragment searchFragment;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_base_drawer);

    LinearLayout contentLayout = (LinearLayout) findViewById(R.id.base__content_layout);
    contentLayout.addView(createView(contentLayout));

    Toolbar toolbar = (Toolbar) findViewById(R.id.base__toolbar);
    setSupportActionBar(toolbar);

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.base__drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
      this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = (NavigationView) findViewById(R.id.base__nav_view);
    navigationView.setNavigationItemSelectedListener(this);

    this.onAfterCreate(savedInstanceState);
  }

  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.base__drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.toolbar, menu);

    final SearchView searchView = (SearchView) menu.findItem(R.id.toolbar__action_search).getActionView();
    initializeSearchView(searchView);

    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.base__drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  private void initializeSearchView(SearchView searchView) {
    View searchContainer = findViewById(R.id.base__search_layout);
    View contentContainer = findViewById(R.id.base__content_layout);

    searchFragment = new SearchFragment();
    searchFragment.bootstrap(searchView, searchContainer, contentContainer);

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    fragmentTransaction.add(R.id.base__search_layout, searchFragment);
    fragmentTransaction.commit();
  }

  /** Gets the view for the activity. */
  public abstract View createView(ViewGroup parent);

  /** Called after the onCreate method. */
  public void onAfterCreate(Bundle savedInstanceState) {}
}
