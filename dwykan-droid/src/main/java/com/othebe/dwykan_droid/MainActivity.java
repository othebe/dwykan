package com.othebe.dwykan_droid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.othebe.dwykan_droid.ui.LoginActivity;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Start the CreateRoom activity.
    Intent i = new Intent(this, LoginActivity.class);
    startActivity(i);
  }
}
