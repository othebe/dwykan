package com.othebe.dwykan_droid.ui.controls.players.youtube;

public interface IYouTubePlayerView {
  void playVideo(String url);
  void pauseVideo();
  void seekVideo(int seekTo);
}
