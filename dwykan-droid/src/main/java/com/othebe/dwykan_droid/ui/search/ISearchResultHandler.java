package com.othebe.dwykan_droid.ui.search;

public interface ISearchResultHandler {
  void onClicked(String url, int searchResultType);
}
