package com.othebe.dwykan_droid.ui.search;

import com.google.api.services.youtube.model.ThumbnailDetails;

public interface ISearchResult {
  int getSearchResultType();
  String getTitle();
  ThumbnailDetails getThumbnails();
  String getDescription();
  String getUrl();
}
