package com.othebe.dwykan_droid.ui.controls.playlist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.api.services.youtube.model.ThumbnailDetails;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.api.services.youtube.model.VideoSnippet;
import com.google.api.services.youtube.model.VideoStatistics;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.common.SafeExceptionHandler;
import com.othebe.dwykan_droid.services.YouTubeService;
import com.othebe.dwykan_droid.session.ISession;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PlaylistItemViewHolder extends RecyclerView.ViewHolder {
  private final YouTubeService youTubeService;
  private final ISession session;

  private String videoUrl;
  private View view;
  private ImageView thumbnailView;
  private TextView titleView;
  private TextView viewCountView;

  public PlaylistItemViewHolder(View itemView, YouTubeService youTubeService, ISession session) {
    super(itemView);

    this.youTubeService = youTubeService;
    this.session = session;

    this.view = itemView;
    this.thumbnailView = (ImageView) itemView.findViewById(R.id.thumbnail);
    this.titleView = (TextView) itemView.findViewById(R.id.title);
    this.viewCountView = (TextView) itemView.findViewById(R.id.views);
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
    loadVideoDetails();
    view.setOnClickListener(onClickListener);
  }

  private void loadVideoDetails() {
    Observable.fromCallable(new Callable<VideoListResponse>() {
      @Override
      public VideoListResponse call() throws Exception {
        return youTubeService.getVideoDetails(videoUrl);
      }
    }).subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(SafeExceptionHandler.getSafeSubscriber(readVideoListResponse()));
  }

  private Action1<VideoListResponse> readVideoListResponse() {
    return new Action1<VideoListResponse>() {
      @Override
      public void call(VideoListResponse videoListResponse) {
        List<Video> videoList = videoListResponse.getItems();
        for (Video video : videoList) {
          VideoSnippet videoSnippet = video.getSnippet();
          VideoStatistics statistics = video.getStatistics();

          // Load thumbnail.
          ThumbnailDetails thumbnailDetails = videoSnippet.getThumbnails();
          String thumbnailUrl = thumbnailDetails.getMedium().getUrl();
          Picasso.with(view.getContext()).load(thumbnailUrl).into(thumbnailView);

          // Title.
          titleView.setText(videoSnippet.getTitle());

          // View count.
          BigInteger viewCount = statistics.getViewCount();
          viewCountView.setText(viewCount.toString() + " views.");
        }
      }
    };
  }

  private final View.OnClickListener onClickListener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      session.playVideo(videoUrl);
    }
  };
}
