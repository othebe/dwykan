package com.othebe.dwykan_droid.ui.createRoom;

import android.content.Context;

/**
 * Created by otheb on 4/28/2016.
 */
public interface ICreateRoomView {
  Context getContext();
  void startConnectionInProgress();
  void endConnectionInProgress();
  void handleConnect();
}
