package com.othebe.dwykan_droid.ui.search.recycler_view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.ui.search.ISearchResult;
import com.othebe.dwykan_droid.ui.search.ISearchResultHandler;

import java.util.List;

import javax.inject.Inject;

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsViewHolder> {
  private List<ISearchResult> searchResultList;
  private ISearchResultHandler searchResultHandler;

  @Inject
  public SearchResultsAdapter() {
  }

  @Override
  public SearchResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result, parent, false);

    return new SearchResultsViewHolder(v, searchResultHandler);
  }

  @Override
  public void onBindViewHolder(SearchResultsViewHolder holder, int position) {
    holder.bind(searchResultList.get(position));
  }

  @Override
  public int getItemCount() {
    if (searchResultList == null) {
      return 0;
    } else {
      return searchResultList.size();
    }
  }

  public void setSearchResultHandler(ISearchResultHandler searchResultHandler) {
    this.searchResultHandler = searchResultHandler;
  }

  public void setData(List<ISearchResult> searchResultList) {
    this.searchResultList = searchResultList;
    notifyDataSetChanged();
  }
}
