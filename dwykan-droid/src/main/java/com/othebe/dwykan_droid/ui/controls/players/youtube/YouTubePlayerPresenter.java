package com.othebe.dwykan_droid.ui.controls.players.youtube;

import com.google.android.youtube.player.YouTubePlayer;
import com.messages.message_types.AckBufferingVideoMessage;
import com.messages.message_types.AckPauseVideoMessage;
import com.messages.message_types.AckPlayVideoMessage;
import com.messages.message_types.AckSeekVideoMessage;
import com.messages.message_types.AckStopVideoMessage;
import com.messages.message_types.ServerPauseVideoMessage;
import com.messages.message_types.ServerPlayVideoMessage;
import com.messages.message_types.ServerSeekVideoMessage;
import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.session.ResponseHandler;
import com.othebe.dwykan_droid.session.ISession;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class YouTubePlayerPresenter {
  private IYouTubePlayerView view;
  private YouTubePlayer youTubePlayer;

  private final ISession session;
  private final IBus bus;

  @Inject
  public YouTubePlayerPresenter(ISession session, IBus bus) {
    this.session = session;
    this.bus = bus;

    setHandlers();
  }

  public void setView(IYouTubePlayerView view) {
    this.view = view;
  }

  private void setHandlers() {
    bus.onEventOf(ServerPlayVideoMessage.class, new Action1<ServerPlayVideoMessage>() {
      @Override
      public void call(ServerPlayVideoMessage serverPlayVideoMessage) {
        view.playVideo(serverPlayVideoMessage.getVideoUrl());
      }
    }, AndroidSchedulers.mainThread());

    bus.onEventOf(ServerPauseVideoMessage.class, new Action1<ServerPauseVideoMessage>() {
      @Override
      public void call(ServerPauseVideoMessage serverPauseVideoMessage) {
        view.pauseVideo();
      }
    }, AndroidSchedulers.mainThread());

    bus.onEventOf(ServerSeekVideoMessage.class, new Action1<ServerSeekVideoMessage>() {
      @Override
      public void call(ServerSeekVideoMessage serverSeekVideoMessage) {
        view.seekVideo(serverSeekVideoMessage.getSeekTo());
      }
    }, AndroidSchedulers.mainThread());
  }
}
