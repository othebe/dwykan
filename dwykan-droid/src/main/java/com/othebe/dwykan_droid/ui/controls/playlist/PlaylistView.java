package com.othebe.dwykan_droid.ui.controls.playlist;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.othebe.dwykan_droid.AppModule;
import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.databinding.PlaylistBinding;
import com.othebe.dwykan_droid.services.ServiceModule;
import com.othebe.dwykan_droid.session.ISession;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Component;

public class PlaylistView extends FrameLayout implements IPlaylistView {
  private PlaylistBinding binding;

  @Inject
  ISession session;

  @Inject
  PlaylistViewPresenter playlistViewPresenter;

  @Inject
  PlaylistAdapter adapter;

  public PlaylistView(Context context) {
    this(context, null, 0);
  }

  public PlaylistView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public PlaylistView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.playlist, this, true);

    DaggerPlaylistView_PlaylistViewComponent.builder()
      .build().inject(this);

    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
    binding.recyclerview.setLayoutManager(linearLayoutManager);
    binding.recyclerview.setAdapter(adapter);

    playlistViewPresenter.setView(this);
  }

  @Override
  public void addVideoUrl(String videoUrl) {
    adapter.addVideoUrl(videoUrl);
  }

  @Singleton
  @Component(modules = {AppModule.class, ServiceModule.class})
  public interface PlaylistViewComponent {
    void inject(PlaylistView playlistView);
  }
}
