package com.othebe.dwykan_droid.ui.search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.othebe.dwykan_droid.AppModule;
import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.helpers.HelperModule;
import com.othebe.dwykan_droid.helpers.SnackbarHelper;
import com.othebe.dwykan_droid.services.ServiceModule;
import com.othebe.dwykan_droid.session.ISession;
import com.othebe.dwykan_droid.ui.search.recycler_view.SearchResultsAdapter;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Component;

public class SearchFragment extends Fragment implements ISearchView {
  @Inject
  ISession session;

  @Inject
  SearchFragmentPresenter presenter;

  @Inject
  SearchResultsAdapter searchResultsAdapter;

  @Inject
  SnackbarHelper snackbarHelper;

  private RecyclerView recyclerView;
  private SearchView searchView;
  private View searchContainer;
  private View contentContainer;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_search, container, false);
  }

  @Override
  public void showSearchResults(List<ISearchResult> searchResultList) {
    searchResultsAdapter.setData(searchResultList);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    inject();

    searchResultsAdapter.setSearchResultHandler(searchResultHandler);
    presenter.attachView(this);

    recyclerView = (RecyclerView) view;
    recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
    recyclerView.setAdapter(searchResultsAdapter);
  }

  public void bootstrap(SearchView searchView, View searchContainer, View contentContainer) {
    this.searchView = searchView;
    this.searchContainer = searchContainer;
    this.contentContainer = contentContainer;

    searchView.setIconifiedByDefault(true);
    searchView.setSubmitButtonEnabled(false);
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        return true;
      }

      @Override
      public boolean onQueryTextChange(String query) {
        query = query.trim();
        if (query.trim().length() > 0) {
          setVisibility(true);
          search(query);
        } else {
          setVisibility(false);
        }

        return false;
      }
    });
  }

  public void setVisibility(boolean isDisplayed) {
    if (isDisplayed) {
      searchContainer.setVisibility(View.VISIBLE);
      contentContainer.setVisibility(View.GONE);
    } else {
      searchContainer.setVisibility(View.GONE);
      contentContainer.setVisibility(View.VISIBLE);
    }
  }

  public void search(String query) {
    presenter.search(query);
  }

  private final ISearchResultHandler searchResultHandler = new ISearchResultHandler() {
    @Override
    public void onClicked(String url, int searchResultType) {
      session.addVideo(url, searchResultType);
      snackbarHelper.makeVideoAddedSnackbar().show();
      searchView.setQuery("", false);
      setVisibility(false);

      InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
  };

  private void inject() {
    DaggerSearchFragment_SearchFragmentPresenterComponent.builder()
      .helperModule(new HelperModule(this.getView()))
      .build().inject(this);
  }

  @Singleton
  @Component(modules = {
    AppModule.class,
    ServiceModule.class,
    HelperModule.class
  })
  public interface SearchFragmentPresenterComponent {
    void inject(SearchFragment searchFragment);
  }
}
