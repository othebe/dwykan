package com.othebe.dwykan_droid.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.othebe.dwykan_droid.AppModule;
import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.ui.createRoom.CreateRoomPresenter;
import com.othebe.dwykan_droid.ui.createRoom.ICreateRoomView;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Component;

public class LoginActivity extends AppCompatActivity implements ICreateRoomView {
  @Inject
  CreateRoomPresenter presenter;

  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_login);

    DaggerLoginActivity_LoginActivityComponent.builder()
      .build().inject(this);

    presenter.setView(this);

    setupCreateRoomBtn();
  }

  @Override
  public Context getContext() {
    return this;
  }

  @Override
  public void endConnectionInProgress() {
    findViewById(R.id.login__progress_container).setVisibility(View.INVISIBLE);
  }

  @Override
  public void handleConnect() {
    Intent intent = new Intent(this, RoomActivity.class);
    startActivity(intent);
  }

  @Override
  public void startConnectionInProgress() {
    findViewById(R.id.login__progress_container).setVisibility(View.VISIBLE);

    Button createBtn = (Button) findViewById(R.id.login__create_room_btn);
    createBtn.setBackgroundResource(R.color.loginBtnDisabled);
    createBtn.setEnabled(false);
  }

  private void setupCreateRoomBtn() {
    Button btn = (Button) findViewById(R.id.login__create_room_btn);
    btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        EditText roomHandleEditText = (EditText) findViewById(R.id.login__room_handle);
        presenter.connectToRoom(roomHandleEditText.toString());
      }
    });
  }

  @Singleton
  @Component(modules = {AppModule.class})
  public interface LoginActivityComponent {
    void inject(LoginActivity loginActivity);
  }
}
