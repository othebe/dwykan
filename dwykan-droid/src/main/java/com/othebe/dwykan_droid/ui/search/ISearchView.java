package com.othebe.dwykan_droid.ui.search;

import java.util.List;

public interface ISearchView {
  void showSearchResults(List<ISearchResult> searchResultList);
}
