package com.othebe.dwykan_droid.ui.search;

import com.othebe.dwykan_droid.services.YouTubeService;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SearchFragmentPresenter {
  private YouTubeService youTubeService;
  private ISearchView view;

  private Observable<List<ISearchResult>> searchObservable;
  private Subscription searchSubscription;

  @Inject
  public SearchFragmentPresenter(YouTubeService youTubeService) {
    this.youTubeService = youTubeService;
  }

  public void attachView(ISearchView view) {
    this.view = view;
  }

  public void search(final String query) {
    searchObservable = Observable.fromCallable(new Callable<List<ISearchResult>>() {
      @Override
      public List<ISearchResult> call() throws Exception {
        return youTubeService.search(query);
      }
    });

    searchObservable
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(getSearchResultHandler());
  }

  private Action1<List<ISearchResult>> getSearchResultHandler() {
    return new Action1<List<ISearchResult>>() {
      @Override
      public void call(List<ISearchResult> searchResults) {
        view.showSearchResults(searchResults);
      }
    };
  }
}
