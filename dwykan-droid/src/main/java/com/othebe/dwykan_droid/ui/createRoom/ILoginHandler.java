package com.othebe.dwykan_droid.ui.createRoom;

public interface ILoginHandler {
  void onClientConnected();
  void onHandleSet();
}
