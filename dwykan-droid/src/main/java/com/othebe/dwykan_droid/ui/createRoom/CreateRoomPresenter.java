package com.othebe.dwykan_droid.ui.createRoom;

import com.messages.message_types.AckClientMessage;
import com.messages.message_types.AckJoinRoomMessage;
import com.messages.message_types.AckSetHandleMessage;
import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan_droid.common.SafeExceptionHandler;
import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.session.ResponseHandler;
import com.othebe.dwykan_droid.session.ISession;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class CreateRoomPresenter {
  final private ISession session;
  final private Client client;
  final private Room room;
  final IBus bus;

  private ICreateRoomView view;

  @Inject
  public CreateRoomPresenter(IBus bus, ISession session, Room room, Client client) {
    this.bus = bus;
    this.session = session;
    this.room = room;
    this.client = client;

    setHandlers();
  }

  public void setView(ICreateRoomView view) {
    this.view = view;
  }

  public void connectToRoom(String roomHandle) {
    view.startConnectionInProgress();

    Observable.fromCallable(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        session.connect();
        return null;
      }
    }).subscribeOn(Schedulers.io())
      .subscribe(SafeExceptionHandler.getSafeSubscriber());
  }

  private void setHandlers() {
    bus.onEventOf(AckClientMessage.class, new Action1<AckClientMessage>() {
      @Override
      public void call(AckClientMessage message) {
        // Once the session is opened, set the client handle.
        String handle = "Ozzy";
        session.setHandle(handle);
        client.setHandle(handle);
      }
    }, Schedulers.io());

    bus.onEventOf(AckSetHandleMessage.class, new Action1<AckSetHandleMessage>() {
      @Override
      public void call(AckSetHandleMessage message) {
        // Once the handle is set, join a room.
        session.joinRoom(1 /** roomId */);
      }
    }, Schedulers.io());

    bus.onEventOf(AckJoinRoomMessage.class, new Action1<AckJoinRoomMessage>() {
      @Override
      public void call(final AckJoinRoomMessage message) {
        Observable.fromCallable(new Callable<Void>() {
          @Override
          public Void call() throws Exception {
            room.setId(message.getRoomId());
            for (String videoUrl : message.getVideoUrls()) {
              room.getRoomVideos().addVideo(videoUrl);
            }

            // All done initializing session!
            view.endConnectionInProgress();
            view.handleConnect();
            return null;
          }
        }).subscribeOn(AndroidSchedulers.mainThread())
          .subscribe(SafeExceptionHandler.getSafeSubscriber());
      }
    }, Schedulers.io());
  }
}
