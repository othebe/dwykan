package com.othebe.dwykan_droid.ui.controls.players.youtube;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.othebe.dwykan.models.room.PlayState;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan.models.room.RoomVideos;
import com.othebe.dwykan_droid.AppModule;
import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.common.Constants;
import com.othebe.dwykan_droid.session.ISession;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Component;

public class YouTubePlayerFragment extends Fragment implements IYouTubePlayerView {
  private YouTubePlayerSupportFragment youTubePlayerFragment;
  private YouTubePlayer youTubePlayer;

  private int lastSeekTime;
  private View playerContainer;

  @Inject
  Room room;

  @Inject
  ISession session;

  @Inject
  YouTubePlayerPresenter youTubePlayerPresenter;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_youtube_player, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    DaggerYouTubePlayerFragment_YouTubePlayerFragmentComponent.builder()
      .build().inject(this);

    youTubePlayerPresenter.setView(this);

    playerContainer = view.findViewById(R.id.youtube_player_container);

    initializeYoutubePlayer();
  }

  private void initializeYoutubePlayer() {
    youTubePlayerFragment = new YouTubePlayerSupportFragment();
    youTubePlayerFragment.initialize(Constants.YOUTUBE_DATA_API_V3_API_KEY, new YouTubePlayer.OnInitializedListener() {
        @Override
        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
          onYouTubePlayerInitialized(youTubePlayer);
        }

        @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
          Log.d("ROOM", "FAILED");
        }
      }
    );

    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
    fragmentTransaction.add(R.id.youtube_player_container, youTubePlayerFragment);
    fragmentTransaction.commit();
  }

  private void onYouTubePlayerInitialized(YouTubePlayer youTubePlayer) {
    this.youTubePlayer = youTubePlayer;
    this.youTubePlayer.setPlaybackEventListener(playbackEventListener);

    String currentUrl = room.getRoomVideos().getCurrentUrl();
    if (currentUrl == null) {
      // If no video is loaded, don't show the player.
      setPlayerVisibility(false);
    } else {
      setPlayerVisibility(true);
    }
  }

  @Override
  public void playVideo(String url) {
    if (youTubePlayer == null) {
      return;
    }

    RoomVideos roomVideos = room.getRoomVideos();

    // If no video loaded, then load this one.
    if (roomVideos.getPlayState() == PlayState.STOPPED) {
      setPlayerVisibility(true);
      roomVideos.setCurrentUrl(url);
      youTubePlayer.loadVideo(url);
    }
    // If a different video is loaded, then load this one.
    else if (roomVideos.getCurrentUrl().compareToIgnoreCase(url) != 0) {
      roomVideos.setCurrentUrl(url);
      youTubePlayer.loadVideo(url);
    }
    // If the video is not playing, resume play.
    else if (roomVideos.getPlayState() != PlayState.PLAYING) {
      youTubePlayer.play();
    }
  }

  @Override
  public void pauseVideo() {
    if (youTubePlayer == null) {
      return;
    }

    PlayState playState = room.getRoomVideos().getPlayState();

    if (playState == PlayState.PLAYING || playState == PlayState.BUFFERING) {
      youTubePlayer.pause();
    }
  }

  @Override
  public void seekVideo(int seekTo) {
    if (youTubePlayer == null) {
      return;
    }

    PlayState playState = room.getRoomVideos().getPlayState();
    if (playState != PlayState.STOPPED && seekTo != lastSeekTime) {
      youTubePlayer.seekToMillis(seekTo);
    }
  }

  private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
    @Override
    public void onPlaying() {
      String videoUrl = room.getRoomVideos().getCurrentUrl();
      room.getRoomVideos().playVideo(videoUrl);
      session.playVideo(videoUrl);
    }

    @Override
    public void onPaused() {
      room.getRoomVideos().pauseVideo();
      session.pauseVideo();
    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int seekTo) {
      lastSeekTime = seekTo;
      room.getRoomVideos().seekVideoTo(seekTo);
      session.seekTo(seekTo);
    }
  };

  private void setPlayerVisibility(boolean isVisible) {
    playerContainer.setVisibility(isVisible ?
      View.VISIBLE : View.GONE);
  }

  @Singleton
  @Component(modules = {AppModule.class})
  public interface YouTubePlayerFragmentComponent {
    public void inject(YouTubePlayerFragment obj);
  }
}
