package com.othebe.dwykan_droid.ui.search.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.ui.search.ISearchResult;
import com.othebe.dwykan_droid.ui.search.ISearchResultHandler;
import com.squareup.picasso.Picasso;

public class SearchResultsViewHolder extends RecyclerView.ViewHolder {
  private ImageView thumbnailView;
  private TextView titleView;
  private Button addButton;

  private ISearchResult searchResult;
  private ISearchResultHandler searchResultHandler;

  public SearchResultsViewHolder(View view, ISearchResultHandler searchResultHandler) {
    super(view);

    this.searchResultHandler = searchResultHandler;

    thumbnailView = (ImageView) view.findViewById(R.id.item_search_result__thumbnail);
    titleView = (TextView) view.findViewById(R.id.item_search_result__title);
    addButton = (Button) view.findViewById(R.id.item_search_result__add_btn);
  }

  public void bind(ISearchResult searchResult) {
    this.searchResult = searchResult;

    // Set title.
    titleView.setText(searchResult.getTitle());

    // Set thumbnail.
    Context context = thumbnailView.getContext();
    String url = searchResult.getThumbnails().getDefault().getUrl();
    Picasso.with(context).load(url).into(thumbnailView);

    // Click listener.
    addButton.setOnClickListener(onAddButtonClickHandler);
  }

  final private View.OnClickListener onAddButtonClickHandler = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      int searchResultType = searchResult.getSearchResultType();
      String url = searchResult.getUrl();

      searchResultHandler.onClicked(url, searchResultType);
    }
  };
}
