package com.othebe.dwykan_droid.ui.controls.playlist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.services.YouTubeService;
import com.othebe.dwykan_droid.session.ISession;

import java.util.ArrayList;

import javax.inject.Inject;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistItemViewHolder> {
  private final YouTubeService youTubeService;
  private final ISession session;

  private ArrayList<String> videoUrlList;

  @Inject
  public PlaylistAdapter(ISession session, YouTubeService youTubeService) {
    this.youTubeService = youTubeService;
    this.session = session;

    this.videoUrlList = new ArrayList<>();
  }

  public void addVideoUrl(String videoUrl) {
    videoUrlList.add(videoUrl);
    notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    return videoUrlList.size();
  }

  @Override
  public PlaylistItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.playlist_item, parent, false);

    return new PlaylistItemViewHolder(view, youTubeService, session);
  }

  @Override
  public void onBindViewHolder(PlaylistItemViewHolder holder, int position) {
    String videoUrl = videoUrlList.get(position);
    holder.setVideoUrl(videoUrl);
  }
}
