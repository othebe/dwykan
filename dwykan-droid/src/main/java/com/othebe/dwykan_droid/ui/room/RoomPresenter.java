package com.othebe.dwykan_droid.ui.room;

import com.google.android.youtube.player.YouTubePlayer;
import com.messages.message_types.AckSeekVideoMessage;
import com.othebe.dwykan_droid.session.ResponseHandler;
import com.othebe.dwykan_droid.session.ISession;

import javax.inject.Inject;

/**
 * Created by otheb on 7/17/2016.
 */
public class RoomPresenter {
  private IRoomView roomView;
  private YouTubePlayer youTubePlayer;

  final private ISession session;

  @Inject
  public RoomPresenter(ISession session) {
    this.session = session;
  }

  public void setView(IRoomView view) {
    this.roomView = view;
  }

  public void onYouTubePlayerInitialized(YouTubePlayer youTubePlayer) {
    this.youTubePlayer = youTubePlayer;
    youTubePlayer.setPlaybackEventListener(youTubePlaybackEventListener);
  }

  private ResponseHandler responseHandler = new ResponseHandler() {
    @Override
    public void onSeek(AckSeekVideoMessage message) {
      super.onSeek(message);
    }
  };

  private final YouTubePlayer.PlaybackEventListener youTubePlaybackEventListener = new YouTubePlayer.PlaybackEventListener() {
    @Override
    public void onPlaying() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int seek) {
      session.seekTo(seek);
    }
  };
}
