package com.othebe.dwykan_droid.ui.controls.playlist;

import com.messages.message_types.ServerAddVideoMessage;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan_droid.common.SafeExceptionHandler;
import com.othebe.dwykan_droid.common.bus.IBus;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PlaylistViewPresenter {
  private final IBus bus;
  private final Room room;

  private IPlaylistView view;

  @Inject
  public PlaylistViewPresenter(final IBus bus, final Room room) {
    this.bus = bus;
    this.room = room;

    setHandlers();
  }

  public void setView(IPlaylistView view) {
    this.view = view;

    loadRoomVideos();
  }

  private void setHandlers() {
    bus.onEventOf(ServerAddVideoMessage.class, new Action1<ServerAddVideoMessage>() {
      @Override
      public void call(final ServerAddVideoMessage serverAddVideoMessage) {
        Observable.fromCallable(new Callable<Void>() {
          @Override
          public Void call() throws Exception {
            view.addVideoUrl(serverAddVideoMessage.getUrl());
            return null;
          }
        }).subscribeOn(AndroidSchedulers.mainThread())
          .subscribe(SafeExceptionHandler.getSafeSubscriber());;
      }
    }, Schedulers.io());
  }

  private void loadRoomVideos() {
    for (String videoUrl : room.getRoomVideos().getVideoUrls()) {
      view.addVideoUrl(videoUrl);
    }
  }
}
