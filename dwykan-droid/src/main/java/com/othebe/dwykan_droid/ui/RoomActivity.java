package com.othebe.dwykan_droid.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.othebe.dwykan_droid.BaseDrawerActivity;
import com.othebe.dwykan_droid.R;
import com.othebe.dwykan_droid.databinding.ContentRoomBinding;

public class RoomActivity extends BaseDrawerActivity {
  private ContentRoomBinding binding;

  @Override
  public View createView(ViewGroup parent) {
    binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.content_room, parent, false);
    setupTabLayout();
    return binding.getRoot();
  }

  @Override
  public void onAfterCreate(Bundle savedInstanceState) {
  }

  private void setupTabLayout() {
    TabLayout tabLayout = binding.roomTabLayout;
    tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0) {
          binding.playlist.setVisibility(View.VISIBLE);
        } else {
          binding.playlist.setVisibility(View.GONE);
        }
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {

      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });
  }
}
