package com.othebe.dwykan_droid.ui.controls.playlist;

public interface IPlaylistView {
  void addVideoUrl(String videoUrl);
}
