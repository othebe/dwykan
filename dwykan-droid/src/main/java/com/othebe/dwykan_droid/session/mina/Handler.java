package com.othebe.dwykan_droid.session.mina;

import com.messages.BaseMessage;
import com.messages.message_types.ClientAddVideoMessage;
import com.messages.message_types.ClientBufferingVideoMessage;
import com.messages.message_types.ClientJoinRoomMessage;
import com.messages.message_types.ClientPauseVideoMessage;
import com.messages.message_types.ClientPlayVideoMessage;
import com.messages.message_types.ClientSeekVideoMessage;
import com.messages.message_types.ClientSetHandleMessage;
import com.messages.message_types.ClientStopVideoMessage;
import com.othebe.dwykan_droid.common.SafeExceptionHandler;
import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.session.ResponseHandler;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import javax.inject.Inject;

public class Handler extends IoHandlerAdapter {
  private IoSession session;

  private IBus bus;

  @Inject
  public Handler(IBus bus) {
    this.bus = bus;
  }

  @Override
  public void sessionOpened(IoSession session) throws Exception {
    this.session = session;
  }

  @Override
  public void messageReceived(IoSession session, Object message) {
    bus.send((BaseMessage) message);
  }

  @Override
  public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
    SafeExceptionHandler.handle(cause);
  }

  protected void disconnect() {
    session.closeNow();
  }

  protected void setHandle(String handle) {
    session.write(new ClientSetHandleMessage(handle));
  }

  protected void joinRoom(long roomId) {
    session.write(new ClientJoinRoomMessage(roomId));
  }

  protected void addVideo(String url, int videoType) {
    session.write(new ClientAddVideoMessage(url, videoType));
  }

  protected void seekTo(int seek) {
    session.write(new ClientSeekVideoMessage(seek));
  }

  protected void playVideo(String videoUrl) {
    session.write(new ClientPlayVideoMessage(videoUrl));
  }

  protected void pauseVideo() {
    session.write(new ClientPauseVideoMessage());
  }

  protected void stopVideo() {
    session.write(new ClientStopVideoMessage());
  }

  protected void bufferingVideo(boolean isBuffering) {
    session.write(new ClientBufferingVideoMessage());
  }
}
