package com.othebe.dwykan_droid.session.mina;

import com.othebe.dwykan.mina.MessageCodecFactory;
import com.othebe.dwykan_droid.AppModule;
import com.othebe.dwykan_droid.common.Constants;
import com.othebe.dwykan_droid.common.SafeExceptionHandler;
import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.session.ResponseHandler;
import com.othebe.dwykan_droid.session.ISession;

import org.apache.mina.core.service.IoConnector;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.net.InetSocketAddress;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Component;

public class MinaSession implements ISession {
  private IoConnector ioConnector;
  private Handler handler;

  @Inject
  public MinaSession(IBus bus) {
    DaggerMinaSession_MinaSessionComponent.builder().build().inject(this);

    this.handler = new Handler(bus);
    ioConnector = new NioSocketConnector();
    ioConnector.getFilterChain().addFirst("protocol", new ProtocolCodecFilter(new MessageCodecFactory()));
    ioConnector.setHandler(handler);
  }

  @Override
  public void connect() {
    try {
      ioConnector.connect(new InetSocketAddress(Constants.HOST, Constants.PORT));
    } catch (Exception e) {
      SafeExceptionHandler.handle(e);
    }
  }

  @Override
  public void disconnect() {
    handler.disconnect();
  }

  @Override
  public void setHandle(String handle) {
    handler.setHandle(handle);
  }

  @Override
  public void joinRoom(long roomId) {
    handler.joinRoom(roomId);
  }

  @Override
  public void addVideo(String videoUrl, int searchResultType) {
    handler.addVideo(videoUrl, searchResultType);
  }

  @Override
  public void seekTo(int seek) {
    handler.seekTo(seek);
  }

  @Override
  public void playVideo(String videoUrl) {
    handler.playVideo(videoUrl);
  }

  @Override
  public void pauseVideo() {
    handler.pauseVideo();
  }

  @Override
  public void bufferingVideo(boolean isBuffering) {
    handler.bufferingVideo(isBuffering);
  }

  @Override
  public void stopVideo() {
    handler.stopVideo();
  }

  @Singleton
  @Component(modules = AppModule.class)
  public interface MinaSessionComponent {
    public void inject(MinaSession obj);
  }

}
