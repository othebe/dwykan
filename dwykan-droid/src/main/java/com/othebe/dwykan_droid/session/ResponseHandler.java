package com.othebe.dwykan_droid.session;

import com.messages.BaseMessage;
import com.messages.message_types.AckAddVideoMessage;
import com.messages.message_types.AckBufferingVideoMessage;
import com.messages.message_types.AckClientMessage;
import com.messages.message_types.AckJoinRoomMessage;
import com.messages.message_types.AckPauseVideoMessage;
import com.messages.message_types.AckPlayVideoMessage;
import com.messages.message_types.AckSeekVideoMessage;
import com.messages.message_types.AckSetHandleMessage;
import com.messages.message_types.AckStopVideoMessage;
import com.messages.message_types.ServerAddVideoMessage;
import com.othebe.dwykan_droid.common.SafeExceptionHandler;

public class ResponseHandler {
  public void handle(BaseMessage message) {
    try {
      switch (message.getMessageType()) {
        case ACK_CLIENT:
          onClientConnected((AckClientMessage) message);
          break;
        case ACK_SET_HANDLE:
          onSetHandle((AckSetHandleMessage) message);
          break;
        case ACK_JOIN_ROOM:
          onJoinRoom((AckJoinRoomMessage) message);
          break;
        case ACK_ADD_VIDEO:
          onAddVideo((AckAddVideoMessage) message);
          break;
        case SERVER_ADD_VIDEO:
          onAddVideo((ServerAddVideoMessage) message);
          break;
        case ACK_SEEK_VIDEO:
          onSeek((AckSeekVideoMessage) message);
          break;
      }
    } catch (Exception e) {
      SafeExceptionHandler.handle(e);
    }
  }

  public void onClientConnected(AckClientMessage message) {}

  public void onSetHandle(AckSetHandleMessage message) {}

  public void onJoinRoom(AckJoinRoomMessage message) {}

  public void onAddVideo(AckAddVideoMessage message) {}

  public void onAddVideo(ServerAddVideoMessage message) {}

  public void onSeek(AckSeekVideoMessage message) {}

  public void onPlayVideo(AckPlayVideoMessage message) {}

  public void onPauseVideo(AckPauseVideoMessage message) {}

  public void onStopVideo(AckStopVideoMessage message) {}

  public void onBufferingVideo(AckBufferingVideoMessage message) {}
}
