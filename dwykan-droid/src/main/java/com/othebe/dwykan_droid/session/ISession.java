package com.othebe.dwykan_droid.session;

public interface ISession {
  void connect();
  void disconnect();

  void setHandle(String handle);
  void joinRoom(long roomId);

  /** Video stuff */
  void addVideo(String videoUrl, int searchResultType);
  void seekTo(int seek);
  void playVideo(String videoUrl);
  void pauseVideo();
  void stopVideo();
  void bufferingVideo(boolean isBuffering);
}
