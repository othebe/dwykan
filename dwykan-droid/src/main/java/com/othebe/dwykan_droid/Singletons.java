package com.othebe.dwykan_droid;

import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.common.bus.rxbus.RxBus;
import com.othebe.dwykan_droid.models.room.RoomServerListener;
import com.othebe.dwykan_droid.models.room.RoomUpdateHandler;
import com.othebe.dwykan_droid.session.ISession;
import com.othebe.dwykan_droid.session.mina.MinaSession;

public class Singletons {
  private static IBus bus;
  private static ISession session;
  private static Room room;
  private static Client client;

  public static IBus getBus() {
    bus = (bus == null) ? new RxBus() : bus;
    return bus;
  }

  public static ISession getSession(IBus bus) {
    session = (session == null) ? new MinaSession(bus) : session;
    return session;
  }

  public static Room getRoom(IBus bus) {
    if (room == null) {
      room = new Room(0);
      room.setRoomUpdateHandler(new RoomUpdateHandler(bus));
      new RoomServerListener(bus, room);
    }
    return room;
  }

  public static Client getClient() {
    client = (client == null) ? new Client() : client;
    return client;
  }
}
