package com.othebe.dwykan_droid;

import com.othebe.dwykan_droid.services.ServiceModule;

import dagger.Component;

@Component(
  modules = {
    AppModule.class,
    ServiceModule.class
  }
)
public interface AppComponent {
  void inject(BaseDrawerActivity activity);
}
