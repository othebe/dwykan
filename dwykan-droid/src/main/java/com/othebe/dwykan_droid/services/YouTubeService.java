package com.othebe.dwykan_droid.services;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.LowLevelHttpRequest;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.SearchResultSnippet;
import com.google.api.services.youtube.model.ThumbnailDetails;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.gson.GsonBuilder;
import com.othebe.dwykan_droid.common.Constants;
import com.othebe.dwykan_droid.ui.search.ISearchResult;
import com.othebe.dwykan_droid.ui.search.SearchResultType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

public class YouTubeService {
  @Inject
  public YouTubeService() {
  }

  /**
   * Determines if the user has authenticated with YouTube.
   * @return
   */
  public boolean isUserAuthenticated() {
    return false;
  }

  /**
   * Search Youtube.
   * @param query
   * @return
   * @throws IOException
   */
  public List<ISearchResult> search(String query) throws IOException {
    YouTube youTube = new YouTube.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null)
      .setApplicationName(Constants.APPLICATION_NAME).build();
    YouTube.Search.List search = youTube.search().list("id, snippet");
    search.setQ(query);
    search.setType("video");
    if (isUserAuthenticated()) {
      search.setOauthToken(getOAuthToken());
    } else {
      search.setKey(Constants.YOUTUBE_DATA_API_V3_API_KEY);
    }

    SearchListResponse searchListResponse = search.execute();

    return Observable.from(searchListResponse.getItems())
      .map(new Func1<SearchResult, ISearchResult>() {
        @Override
        public ISearchResult call(SearchResult searchResult) {
          return convertSearchResults(searchResult);
        }
      })
      .toList()
      .toBlocking()
      .single();
  }

  public VideoListResponse getVideoDetails(String videoId) throws IOException {
    YouTube youTube = new YouTube.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null)
      .setApplicationName(Constants.APPLICATION_NAME).build();
    YouTube.Videos.List search = youTube.videos().list("snippet,statistics");
    search.setId(videoId);
    search.setKey(Constants.YOUTUBE_DATA_API_V3_API_KEY);

    return search.execute();
  }

  private ISearchResult convertSearchResults(final SearchResult searchResult) {
    return new ISearchResult() {
      private SearchResultSnippet snippet = searchResult.getSnippet();

      @Override
      public int getSearchResultType() {
        return SearchResultType.YOUTUBE_VIDEO;
      }

      @Override
      public String getTitle() {
        return snippet.getTitle();
      }

      @Override
      public ThumbnailDetails getThumbnails() {
        return snippet.getThumbnails();
      }

      @Override
      public String getDescription() {
        return snippet.getDescription();
      }

      @Override
      public String getUrl() {
        return searchResult.getId().getVideoId().toString();
      }
    };
  }

  private String getOAuthToken() {
    return null;
  }
}
