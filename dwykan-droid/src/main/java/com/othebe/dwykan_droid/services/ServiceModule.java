package com.othebe.dwykan_droid.services;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {
  @Provides
  public YouTubeService provideYoutubeService() {
    return new YouTubeService();
  }
}
