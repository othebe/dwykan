package com.othebe.dwykan_droid.models.room;

import com.messages.message_types.ServerAddVideoMessage;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan_droid.common.bus.IBus;

import javax.inject.Inject;

import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class RoomServerListener {
  @Inject
  public RoomServerListener(final IBus bus, final Room room) {
    bus.onEventOf(ServerAddVideoMessage.class, new Action1<ServerAddVideoMessage>() {
      @Override
      public void call(ServerAddVideoMessage serverAddVideoMessage) {
        room.getRoomVideos().addVideo(serverAddVideoMessage.getUrl());
      }
    }, Schedulers.io());
  }
}
