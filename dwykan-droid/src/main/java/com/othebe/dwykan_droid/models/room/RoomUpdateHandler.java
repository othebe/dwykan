package com.othebe.dwykan_droid.models.room;

import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.IRoomUpdateHandler;
import com.othebe.dwykan_droid.common.bus.IBus;

import javax.inject.Inject;

public class RoomUpdateHandler implements IRoomUpdateHandler {
  private final IBus bus;

  @Inject
  public RoomUpdateHandler(IBus bus) {
    this.bus = bus;
  }

  @Override
  public void onAddClient(Client client) {
    bus.send(RoomUpdateMessage.createUpdateClientListMessage());
  }

  @Override
  public void onRemoveClient(Client client) {
    bus.send(RoomUpdateMessage.createUpdateClientListMessage());
  }

  @Override
  public void onAddVideo(String url) {
    bus.send(RoomUpdateMessage.createUpdateVideoListMessage());
  }

  @Override
  public void onBuffering() {
    bus.send(RoomUpdateMessage.createUpdateVideoStateMessage());
  }

  @Override
  public void onPauseVideo() {
    bus.send(RoomUpdateMessage.createUpdateVideoStateMessage());
  }

  @Override
  public void onPlayVideo(String url) {
    bus.send(RoomUpdateMessage.createUpdateVideoStateMessage());
  }

  @Override
  public void onSeekVideo(int seekTo) {
    bus.send(RoomUpdateMessage.createUpdateVideoStateMessage());
  }

  @Override
  public void onStopVideo() {
    bus.send(RoomUpdateMessage.createUpdateVideoStateMessage());
  }
}
