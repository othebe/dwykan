package com.othebe.dwykan_droid.models.room;

public class RoomUpdateMessage {
  public static final int UPDATE_CLIENT_LIST = 1;
  public static final int UPDATE_VIDEO_LIST = 2;
  public static final int UPDATE_VIDEO_STATE = 3;

  private final int updateType;

  private RoomUpdateMessage(int updateType) {
    this.updateType = updateType;
  }

  public int getUpdateType() {
    return updateType;
  }

  public static RoomUpdateMessage createUpdateClientListMessage() {
    return new RoomUpdateMessage(UPDATE_CLIENT_LIST);
  }

  public static RoomUpdateMessage createUpdateVideoListMessage() {
    return new RoomUpdateMessage(UPDATE_VIDEO_LIST);
  }

  public static RoomUpdateMessage createUpdateVideoStateMessage() {
    return new RoomUpdateMessage(UPDATE_VIDEO_STATE);
  }
}
