package com.othebe.dwykan_droid;

import com.othebe.dwykan.models.client.Client;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.common.bus.rxbus.RxBus;
import com.othebe.dwykan_droid.session.ISession;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
  @Provides
  @Singleton
  public IBus providesRxBus() {
    return Singletons.getBus();
  }

  @Provides
  @Singleton
  public ISession providesSession(IBus bus) {
    return Singletons.getSession(bus);
  }

  @Provides
  @Singleton
  public Room providesRoom(IBus bus) {
    return Singletons.getRoom(bus);
  }

  @Provides
  @Singleton
  public Client providesClient() {
    return Singletons.getClient();
  }
}
