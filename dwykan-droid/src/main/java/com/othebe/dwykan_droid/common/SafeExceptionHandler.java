package com.othebe.dwykan_droid.common;

import android.util.Log;

import rx.Subscriber;
import rx.functions.Action1;

/**
 * Created by otheb on 4/28/2016.
 */
public class SafeExceptionHandler {
  public static void handle(Exception e) {
    Log.e("Dwykan err", e.toString());
  }

  public static void handle(Throwable e) {
    Log.e("Dwykan err", e.getMessage());
  }

  public static Subscriber getSafeSubscriber() {
    return new Subscriber() {
      @Override
      public void onError(Throwable e) {
        SafeExceptionHandler.handle(e);
      }

      @Override
      public void onCompleted() {}

      @Override
      public void onNext(Object o) {}
    };
  }

  public static <T> Subscriber<T> getSafeSubscriber(final Action1<T> onCompleted) {
    return new Subscriber<T>() {
      @Override
      public void onNext(T o) {
        onCompleted.call(o);
      }

      @Override
      public void onError(Throwable e) {
        SafeExceptionHandler.handle(e);
      }

      @Override
      public void onCompleted() {}
    };
  }
}
