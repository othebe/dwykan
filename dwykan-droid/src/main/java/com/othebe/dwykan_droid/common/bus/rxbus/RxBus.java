package com.othebe.dwykan_droid.common.bus.rxbus;

import com.othebe.dwykan_droid.common.bus.IBus;
import com.othebe.dwykan_droid.common.bus.IBusEventHandler;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

public class RxBus implements IBus {
  private final Subject bus = new SerializedSubject(PublishSubject.create());

  @Override
  public void send(Object event) {
    bus.onNext(event);
  }

  @Override
  public <T> IBusEventHandler onEventOf(Class<T> klass, Action1<T> onEvent, Scheduler scheduler) {
    RxBusEventHandlerBase rxBusEventHandlerBase = new RxBusEventHandlerBase(this);
    rxBusEventHandlerBase.onEventOf(klass, onEvent, scheduler);

    return rxBusEventHandlerBase;
  }

  public <T> Observable getObservableForType(Class<T> klass) {
    return bus.ofType(klass);
  }
}
