package com.othebe.dwykan_droid.common.bus;

import rx.Scheduler;
import rx.functions.Action1;

/**
 * Created by otheb on 4/30/2016.
 */
public interface IBus {
  void send(Object event);
  <T> IBusEventHandler onEventOf(Class<T> klass, Action1<T> onEvent, Scheduler scheduler);
}
