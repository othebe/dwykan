package com.othebe.dwykan_droid.common.bus;

import rx.Scheduler;
import rx.functions.Action1;

public interface IBusEventHandler {
  <T> void onEventOf(Class<T> klass, Action1<T> onEvent, Scheduler scheduler);
  void unsubscribe();
}
