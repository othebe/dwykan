package com.othebe.dwykan_droid.common.bus.rxbus;

import com.othebe.dwykan_droid.common.SafeExceptionHandler;
import com.othebe.dwykan_droid.common.bus.IBusEventHandler;

import java.util.concurrent.Callable;

import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class RxBusEventHandlerBase implements IBusEventHandler {
  private RxBus rxBus;
  private Subscription subscription;

  public RxBusEventHandlerBase(RxBus rxBus) {
    this.rxBus = rxBus;
  }

  @Override
  public <T> void onEventOf(Class<T> klass, Action1<T> onEvent, Scheduler scheduler) {
    subscription = rxBus.getObservableForType(klass)
      .subscribeOn(Schedulers.io())
      .observeOn(scheduler)
      .subscribe(SafeExceptionHandler.getSafeSubscriber(onEvent));
  }

  @Override
  public void unsubscribe() {
    if (subscription != null) {
      subscription.unsubscribe();
    }
  }
}
