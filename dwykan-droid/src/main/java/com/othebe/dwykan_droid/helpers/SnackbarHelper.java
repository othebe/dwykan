package com.othebe.dwykan_droid.helpers;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.othebe.dwykan_droid.R;

import javax.inject.Inject;

public class SnackbarHelper {
  private final View coordinatorLayout;

  @Inject
  public SnackbarHelper(View coordinatorLayout) {
    this.coordinatorLayout = coordinatorLayout;
  }

  public Snackbar makeVideoAddedSnackbar() {
    return Snackbar.make(coordinatorLayout, R.string.video_added, Snackbar.LENGTH_SHORT);
  }
}
