package com.othebe.dwykan_droid.helpers;

import android.support.design.widget.CoordinatorLayout;
import android.view.View;

import com.othebe.dwykan_droid.R;

import dagger.Module;
import dagger.Provides;

@Module
public class HelperModule {
  private CoordinatorLayout coordinatorLayout;

  public HelperModule(View view) {
    View rootView = view.getRootView();
    if (rootView instanceof CoordinatorLayout) {
      coordinatorLayout = (CoordinatorLayout) rootView;
    } else {
      coordinatorLayout = (CoordinatorLayout) view.getRootView().findViewById(R.id.coordinator_layout);
    }
  }

  @Provides
  public SnackbarHelper provideSnackbarService() {
    return new SnackbarHelper(coordinatorLayout);
  }
}
