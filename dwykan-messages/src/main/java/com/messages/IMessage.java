package com.messages;

/**
 * Created by otheb on 4/28/2016.
 */
public interface IMessage {
  MessageType getMessageType();
}
