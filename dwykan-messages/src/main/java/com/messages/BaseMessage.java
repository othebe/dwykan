package com.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;

import java.lang.reflect.Type;

/**
 * Created by otheb on 4/28/2016.
 */
public abstract class BaseMessage implements IMessage {
  private MessageType messageType;

  public BaseMessage(MessageType messageType) {
    this.messageType = messageType;
  }

  @Override
  public MessageType getMessageType() {
    return messageType;
  }
}
