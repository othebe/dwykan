package com.messages;

/**
 * Created by otheb on 4/30/2016.
 */
public abstract class BaseAckMessage extends BaseMessage {
  protected long errorCode = 0L;

  public BaseAckMessage(MessageType messageType) {
    super(messageType);
  }

  public void setErrorCode(long errorCode) {
    this.errorCode = errorCode;
  }

  public boolean isError() {
    return errorCode > 0;
  };

  public long getErrorCode() {
    return errorCode;
  }
}
