package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 4/30/2016.
 */
public class ClientJoinRoomMessage extends BaseMessage {
  private long roomId;

  public ClientJoinRoomMessage(long roomId) {
    super(MessageType.CLIENT_JOIN_ROOM);
    this.roomId = roomId;
  }

  public long getRoomId() {
    return roomId;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.format("%s\n", super.toString()));
    stringBuilder.append(String.format("Room ID: %s\n", roomId));
    return stringBuilder.toString();
  }
}
