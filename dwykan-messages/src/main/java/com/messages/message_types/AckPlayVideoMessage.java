package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class AckPlayVideoMessage extends BaseAckMessage {
  public AckPlayVideoMessage() {
    super(MessageType.ACK_PLAY_VIDEO);
  }
}
