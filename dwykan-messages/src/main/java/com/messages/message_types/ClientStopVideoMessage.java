package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class ClientStopVideoMessage extends BaseMessage {
  public ClientStopVideoMessage() {
    super(MessageType.CLIENT_STOP_VIDEO);
  }
}
