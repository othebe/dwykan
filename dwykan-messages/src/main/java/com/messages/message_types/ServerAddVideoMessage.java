package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

public class ServerAddVideoMessage extends BaseMessage {
  private final String url;

  public ServerAddVideoMessage(String url) {
    super(MessageType.SERVER_ADD_VIDEO);
    this.url = url;
  }

  public String getUrl() {
    return url;
  }
}
