package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class AckPauseVideoMessage extends BaseAckMessage {
  public AckPauseVideoMessage() {
    super(MessageType.ACK_PAUSE_VIDEO);
  }
}
