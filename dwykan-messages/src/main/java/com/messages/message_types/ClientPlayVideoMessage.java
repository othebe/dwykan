package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

public class ClientPlayVideoMessage extends BaseMessage {
  String videoUrl;

  public ClientPlayVideoMessage(String videoUrl) {
    super(MessageType.CLIENT_PLAY_VIDEO);

    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }
}
