package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 9/5/2016.
 */
public class ServerPauseVideoMessage extends BaseMessage {
  public ServerPauseVideoMessage() {
    super(MessageType.SERVER_PAUSE_VIDEO);
  }
}
