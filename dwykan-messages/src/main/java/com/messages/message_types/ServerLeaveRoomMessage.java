package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;
import com.othebe.dwykan.models.client.Client;

/**
 * Created by otheb on 9/5/2016.
 */
public class ServerLeaveRoomMessage extends BaseMessage {
  public ServerLeaveRoomMessage(Client client) {
    super(MessageType.SERVER_LEAVE_ROOM);
  }
}
