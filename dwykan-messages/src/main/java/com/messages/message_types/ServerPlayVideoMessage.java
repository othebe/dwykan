package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 9/5/2016.
 */
public class ServerPlayVideoMessage extends BaseMessage {
  private final String url;

  public ServerPlayVideoMessage(String url) {
    super(MessageType.SERVER_PLAY_VIDEO);
    this.url = url;
  }

  public String getVideoUrl() {
    return url;
  }
}
