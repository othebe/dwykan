package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class ClientPauseVideoMessage extends BaseMessage {
  public ClientPauseVideoMessage() {
    super(MessageType.CLIENT_PAUSE_VIDEO);
  }
}
