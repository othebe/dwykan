package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

public class ClientSeekVideoMessage extends BaseMessage {
  private int seek;

  public ClientSeekVideoMessage(int seek) {
    super(MessageType.CLIENT_SEEK_VIDEO);

    this.seek = seek;
  }

  public int getSeek() {
    return seek;
  }
}
