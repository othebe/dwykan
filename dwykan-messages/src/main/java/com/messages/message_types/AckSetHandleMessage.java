package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 4/30/2016.
 */
public class AckSetHandleMessage extends BaseAckMessage {
  public AckSetHandleMessage() {
    super(MessageType.ACK_SET_HANDLE);
  }
}
