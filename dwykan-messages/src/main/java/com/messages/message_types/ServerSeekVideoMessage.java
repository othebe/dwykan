package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

public class ServerSeekVideoMessage extends BaseMessage {
  private final int seekTo;

  public ServerSeekVideoMessage(int seekTo) {
    super(MessageType.SERVER_SEEK_VIDEO);
    this.seekTo = seekTo;
  }

  public int getSeekTo() {
    return seekTo;
  }
}
