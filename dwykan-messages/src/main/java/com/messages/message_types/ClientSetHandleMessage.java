package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 4/28/2016.
 */
public class ClientSetHandleMessage extends BaseMessage {
  private String handle;

  public ClientSetHandleMessage(String handle) {
    super(MessageType.CLIENT_SET_HANDLE);
    this.handle = handle;
  }

  public String getHandle() {
    return handle;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.format("%s\n", super.toString()));
    stringBuilder.append(String.format("Handle: %s\n", handle));
    return stringBuilder.toString();
  }
}
