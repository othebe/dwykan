package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/1/2016.
 */
public class ClientAddVideoMessage extends BaseMessage {
  private String videoUrl;
  int videoType;

  public ClientAddVideoMessage(String videoUrl, int videoType) {
    super(MessageType.CLIENT_ADD_VIDEO);
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public int getVideoType() { return videoType; }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.format("%s\n", super.toString()));
    stringBuilder.append(String.format("Video URL: %s\n", videoUrl));

    return stringBuilder.toString();
  }
}
