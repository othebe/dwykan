package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;
import com.othebe.dwykan.models.client.Client;

/**
 * Created by otheb on 9/5/2016.
 */
public class ServerJoinRoomMessage extends BaseMessage {
  private final Client client;

  public ServerJoinRoomMessage(Client client) {
    super(MessageType.SERVER_JOIN_ROOM);

    this.client = client;
  }

  public Client getClient() {
    return client;
  }
}
