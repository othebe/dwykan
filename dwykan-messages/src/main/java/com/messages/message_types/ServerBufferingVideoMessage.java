package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 9/5/2016.
 */
public class ServerBufferingVideoMessage extends BaseMessage {
  public ServerBufferingVideoMessage() {
    super(MessageType.SERVER_BUFFERING_VIDEO);
  }
}
