package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 9/5/2016.
 */
public class ServerStopVideoMessage extends BaseMessage {
  public ServerStopVideoMessage() {
    super(MessageType.SERVER_STOP_VIDEO);
  }
}
