package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;
import com.othebe.dwykan.models.room.Room;
import com.othebe.dwykan.models.room.RoomVideos;

import java.util.List;

/**
 * Created by otheb on 4/30/2016.
 */
public class AckJoinRoomMessage extends BaseAckMessage {
  private long roomId;
  private List<String> videoUrls;

  public AckJoinRoomMessage(Room room) {
    super(MessageType.ACK_JOIN_ROOM);

    this.roomId = room.getId();
    this.videoUrls = room.getRoomVideos().getVideoUrls();;
  }

  public long getRoomId() {
    return roomId;
  }

  public List<String> getVideoUrls() {
    return videoUrls;
  }
}
