package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class AckStopVideoMessage extends BaseAckMessage {
  public AckStopVideoMessage() {
    super(MessageType.ACK_STOP_VIDEO);
  }
}
