package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class AckSeekVideoMessage extends BaseAckMessage {
  private int seekMillis;

  public AckSeekVideoMessage(int seekMillis) {
    super(MessageType.ACK_SEEK_VIDEO);

    this.seekMillis = seekMillis;
  }

  public int getSeekMillis() {
    return seekMillis;
  }
}
