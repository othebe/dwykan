package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class AckBufferingVideoMessage extends BaseAckMessage {
  public AckBufferingVideoMessage() {
    super(MessageType.ACK_BUFFERING_VIDEO);
  }
}
