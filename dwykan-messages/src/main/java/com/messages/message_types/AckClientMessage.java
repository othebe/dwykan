package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 4/28/2016.
 */
public class AckClientMessage extends BaseAckMessage {
  public AckClientMessage() {
    super(MessageType.ACK_CLIENT);
  }
}
