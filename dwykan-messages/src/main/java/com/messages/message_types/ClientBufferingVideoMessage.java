package com.messages.message_types;

import com.messages.BaseMessage;
import com.messages.MessageType;

/**
 * Created by otheb on 7/17/2016.
 */
public class ClientBufferingVideoMessage extends BaseMessage {
  public ClientBufferingVideoMessage() {
    super(MessageType.CLIENT_BUFFERING_VIDEO);
  }
}
