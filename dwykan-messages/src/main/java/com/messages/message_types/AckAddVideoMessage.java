package com.messages.message_types;

import com.messages.BaseAckMessage;
import com.messages.MessageType;
import com.othebe.dwykan.models.room.Room;

/**
 * Created by otheb on 7/4/2016.
 */
public class AckAddVideoMessage extends BaseAckMessage {
  public AckAddVideoMessage(Room room) {
    super(MessageType.ACK_ADD_VIDEO);
  }
}
