package com.messages;

public enum MessageType {
  // Acknowledge client. Sent when session is created.
  ACK_CLIENT,

  // Set client handle.
  CLIENT_SET_HANDLE,
  ACK_SET_HANDLE,

  // Join room.
  CLIENT_JOIN_ROOM,
  ACK_JOIN_ROOM,
  SERVER_JOIN_ROOM,

  // Leave room.
  SERVER_LEAVE_ROOM,

  // Add video.
  CLIENT_ADD_VIDEO,
  ACK_ADD_VIDEO,
  SERVER_ADD_VIDEO,

  // Play a video.
  CLIENT_PLAY_VIDEO,
  ACK_PLAY_VIDEO,
  SERVER_PLAY_VIDEO,

  // Pause a video.
  CLIENT_PAUSE_VIDEO,
  ACK_PAUSE_VIDEO,
  SERVER_PAUSE_VIDEO,

  // Stop a video.
  CLIENT_STOP_VIDEO,
  ACK_STOP_VIDEO,
  SERVER_STOP_VIDEO,

  // Buffering a video.
  CLIENT_BUFFERING_VIDEO,
  ACK_BUFFERING_VIDEO,
  SERVER_BUFFERING_VIDEO,

  // Seek video.
  CLIENT_SEEK_VIDEO,
  ACK_SEEK_VIDEO,
  SERVER_SEEK_VIDEO
}
